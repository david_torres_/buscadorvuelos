package es.udc.med.team04.buscadorvuelos;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;

@SpringBootApplication
@Configuration
@ComponentScan({ "es.udc.med.team04.buscadorvuelos.model" })
public class App {

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost/fs");
		dataSource.setUsername("fs");
		dataSource.setPassword("fs");
		return dataSource;
	}

	@Bean
	public SessionFactory sessionFactory() {

		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(
				dataSource());
		builder.scanPackages("es.udc.med.team04.buscadorvuelos.model");
		builder.addResource("hibernate-config.xml");

		return builder.buildSessionFactory();
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
