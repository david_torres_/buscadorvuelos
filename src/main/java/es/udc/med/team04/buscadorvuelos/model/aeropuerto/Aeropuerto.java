package es.udc.med.team04.buscadorvuelos.model.aeropuerto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Proxy;

@Entity
@Proxy(lazy = false)
public class Aeropuerto {

	private Long aeroId;
	private String nombre;
	private String direccion;
	private String pais;
	private String nombreCorto;

	public Aeropuerto() {

	}

	public Aeropuerto(String nombre, String direccion, String pais,
			String nombreCorto) {

		this.nombre = nombre;
		this.direccion = direccion;
		this.pais = pais;
		this.nombreCorto = nombreCorto;

	}

	@Column(name = "aeroId")
	@SequenceGenerator( // It only takes effect for
	name = "AeroIdGenerator", // databases providing identifier
	sequenceName = "AeroSeq")
	// generators.
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "AeroIdGenerator")
	public Long getAeroId() {
		return aeroId;
	}

	public void setAeroId(Long aeroId) {
		this.aeroId = aeroId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

}
