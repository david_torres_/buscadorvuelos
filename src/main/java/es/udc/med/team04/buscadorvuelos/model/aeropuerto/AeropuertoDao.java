package es.udc.med.team04.buscadorvuelos.model.aeropuerto;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDao;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;


public interface AeropuertoDao extends GenericDao<Aeropuerto, Long> {

	public Aeropuerto findAeropuertoByNombreCorto(String nombreCorto)
			throws InstanceNotFoundException;

}
