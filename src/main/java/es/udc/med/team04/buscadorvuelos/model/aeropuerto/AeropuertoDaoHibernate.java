package es.udc.med.team04.buscadorvuelos.model.aeropuerto;

import org.springframework.stereotype.Repository;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDaoHibernate;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;


@Repository("AeropuertoDao")
public class AeropuertoDaoHibernate extends
		GenericDaoHibernate<Aeropuerto, Long> implements AeropuertoDao {

	public Aeropuerto findAeropuertoByNombreCorto(String nombreCorto)
			throws InstanceNotFoundException {

		Aeropuerto aeropuerto = (Aeropuerto) getSession()
				.createQuery(
						"SELECT u FROM Aeropuerto u WHERE u.nombreCorto = :nombreCorto")
				.setParameter("nombreCorto", nombreCorto).uniqueResult();
		if (aeropuerto == null) {
			throw new InstanceNotFoundException(nombreCorto,
					Aeropuerto.class.getName());
		} else {
			return aeropuerto;
		}
	}
}
