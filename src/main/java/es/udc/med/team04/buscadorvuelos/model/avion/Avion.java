package es.udc.med.team04.buscadorvuelos.model.avion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Proxy;

@Entity
@Proxy(lazy = false)
public class Avion {

	private Long avionId;
	private String nombre;
	private int numPlazasNormalVent;
	private int numPlazasNormalPas;
	private int numPlazasVipVent;
	private int numPlazasVipPas;

	public Avion() {
	}

	public Avion(String nombre, int numPlazasNormal, int numPlazasVip) {

		this.nombre = nombre;
		this.numPlazasNormalPas = numPlazasNormal / 2;
		this.numPlazasNormalVent = numPlazasNormal / 2;
		this.numPlazasVipPas = numPlazasVip / 2;
		this.numPlazasVipVent = numPlazasVip / 2;
	}

	@Column(name = "avionId")
	@SequenceGenerator( // It only takes effect for
	name = "AvionIdGenerator", // databases providing identifier
	sequenceName = "AvionSeq")
	// generators.
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "AvionIdGenerator")
	public Long getAvionId() {
		return avionId;
	}

	public void setAvionId(Long avionId) {
		this.avionId = avionId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumPlazasNormalVent() {
		return numPlazasNormalVent;
	}

	public void setNumPlazasNormalVent(int numPlazasNormalVent) {
		this.numPlazasNormalVent = numPlazasNormalVent;
	}

	public int getNumPlazasNormalPas() {
		return numPlazasNormalPas;
	}

	public void setNumPlazasNormalPas(int numPlazasNormalPas) {
		this.numPlazasNormalPas = numPlazasNormalPas;
	}

	public int getNumPlazasVipVent() {
		return numPlazasVipVent;
	}

	public void setNumPlazasVipVent(int numPlazasVipVent) {
		this.numPlazasVipVent = numPlazasVipVent;
	}

	public int getNumPlazasVipPas() {
		return numPlazasVipPas;
	}

	public void setNumPlazasVipPas(int numPlazasVipPas) {
		this.numPlazasVipPas = numPlazasVipPas;
	}

}
