package es.udc.med.team04.buscadorvuelos.model.avion;

import java.util.List;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDao;


public interface AvionDao extends GenericDao<Avion, Long> {

	public List<Avion> getAllAvion();
}
