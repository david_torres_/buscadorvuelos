package es.udc.med.team04.buscadorvuelos.model.avion;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDaoHibernate;


@Repository("AvionDao")
public class AvionDaoHibernate extends GenericDaoHibernate<Avion, Long>
		implements AvionDao {

	@SuppressWarnings("unchecked")
	public List<Avion> getAllAvion() {

		return getSession().createQuery("SELECT u FROM Avion u").list();
	}

}
