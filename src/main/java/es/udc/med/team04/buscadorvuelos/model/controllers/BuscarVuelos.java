package es.udc.med.team04.buscadorvuelos.model.controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;
import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;
import es.udc.med.team04.buscadorvuelos.model.vueloservice.VueloService;

@RestController
public class BuscarVuelos {

	@Autowired
	private VueloService vueloService;

	@RequestMapping("/busquedaVuelos")
	public List<Vuelo> busquedaVuelos(
			@RequestParam(value = "origen") Long origenId,
			@RequestParam(value = "destino") Long destinoId,
			@RequestParam(value = "fechaIda") String fechaIda,
			@RequestParam(value = "fechaVuelta") String fechaVuelta,
			@RequestParam(value = "clase") String clase,
			@RequestParam(value = "plaza") String plaza,
			@RequestParam(value = "startIndex") int startIndex,
			@RequestParam(value = "count") int count) throws Exception {

		Calendar fechaIdaCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"));
		SimpleDateFormat fecha = new SimpleDateFormat("yyyy/MM/dd", Locale.ROOT);
		fechaIdaCalendar.setTime(fecha.parse(fechaIda));
		Calendar fechaVueltaCalendar;
		if (fechaVuelta.isEmpty()) {
			fechaVueltaCalendar = null;
		} else {
			fechaVueltaCalendar = Calendar.getInstance();
			fechaIdaCalendar.setTime(fecha.parse(fechaVuelta));
		}

		return vueloService.buscarVuelos(origenId, destinoId, fechaIdaCalendar,
				fechaVueltaCalendar, clase, plaza, startIndex, count);
	}
	
	@RequestMapping("/buscarVuelo")
	public Vuelo buscarVueloId(@RequestParam(value = "id") Long vueloId) throws InstanceNotFoundException{
		
		return vueloService.findById(vueloId);
		
	}
}
