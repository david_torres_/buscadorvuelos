package es.udc.med.team04.buscadorvuelos.model.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.med.team04.buscadorvuelos.model.reservaservice.ReservaService;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;

@RestController
public class CancelarReservas {

	@Autowired
	private ReservaService reservaService;

	@RequestMapping("/cancelarReserva")
	public boolean reservaVuelo(
			@RequestParam(value = "reservaId") Long reservaId,
			@RequestParam(value = "dni") String dni)
			throws InstanceNotFoundException {

		return reservaService.cancelarReserva(reservaId, dni);

	}

}
