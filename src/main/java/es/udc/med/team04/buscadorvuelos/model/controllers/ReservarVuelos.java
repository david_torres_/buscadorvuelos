package es.udc.med.team04.buscadorvuelos.model.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.med.team04.buscadorvuelos.model.reserva.Reserva;
import es.udc.med.team04.buscadorvuelos.model.reservaservice.ReservaService;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;

@RestController
public class ReservarVuelos {

	@Autowired
	private ReservaService reservaService;

	@RequestMapping("/reservaVuelo")
	public Reserva reservaVuelo(@RequestParam(value = "ida") Long ida,
			@RequestParam(value = "vuelta") Long vuelta,
			@RequestParam(value = "nombre") String nombre,
			@RequestParam(value = "apellidos") String apellidos,
			@RequestParam(value = "dni") String dni,
			@RequestParam(value = "telefono") String telefono,
			@RequestParam(value = "clase") String clase,
			@RequestParam(value = "ubicacion") String ubicacion)
			throws InstanceNotFoundException {

		return reservaService.reservar(ida, vuelta, nombre, apellidos, dni,
				telefono, clase, ubicacion);

	}

}
