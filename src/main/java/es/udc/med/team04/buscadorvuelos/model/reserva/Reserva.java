package es.udc.med.team04.buscadorvuelos.model.reserva;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Proxy;

import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;

@Entity
@Proxy(lazy = false)
public class Reserva {

	private Long reservaId;
	private Vuelo ida;
	private Vuelo vuelta;
	private String nombre;
	private String apellidos;
	private String dni;
	private String telefono;
	private String clase;
	private String clase2;
	private String ubicacion;
	private String ubicacion2;
	private Float precio;

	public Reserva() {

	}

	public Reserva(Vuelo ida, Vuelo vuelta, String nombre, String apellidos,
			String dni, String telefono, String clase, String clase2,
			String ubicacion, String ubicacion2, Float precio) {

		this.ida = ida;
		this.vuelta = vuelta;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		this.telefono = telefono;
		this.clase = clase;
		this.clase2 = clase2;
		this.ubicacion = ubicacion;
		this.ubicacion2 = ubicacion2;
		this.precio = precio;
	}

	@Column(name = "reservaId")
	@SequenceGenerator( // It only takes effect for
	name = "ReservaIdGenerator", // databases providing identifier
	sequenceName = "ReservaSeq")
	// generators.
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ReservaIdGenerator")
	public Long getReservaId() {
		return reservaId;
	}

	public void setReservaId(Long reservaId) {
		this.reservaId = reservaId;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ida")
	public Vuelo getIda() {
		return ida;
	}

	public void setIda(Vuelo ida) {
		this.ida = ida;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vuelta")
	public Vuelo getVuelta() {
		return vuelta;
	}

	public void setVuelta(Vuelo vuelta) {
		this.vuelta = vuelta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getClase2() {
		return clase2;
	}

	public void setClase2(String clase2) {
		this.clase2 = clase2;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getUbicacion2() {
		return ubicacion2;
	}

	public void setUbicacion2(String ubicacion2) {
		this.ubicacion2 = ubicacion2;
	}

	@Column(name = "precioBase")
	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

}
