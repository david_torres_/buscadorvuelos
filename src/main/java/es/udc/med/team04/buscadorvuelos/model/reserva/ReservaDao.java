package es.udc.med.team04.buscadorvuelos.model.reserva;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDao;


public interface ReservaDao extends GenericDao<Reserva, Long> {

}
