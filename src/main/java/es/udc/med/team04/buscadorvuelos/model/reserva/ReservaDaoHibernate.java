package es.udc.med.team04.buscadorvuelos.model.reserva;

import org.springframework.stereotype.Repository;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDaoHibernate;


@Repository("ReservaDao")
public class ReservaDaoHibernate extends GenericDaoHibernate<Reserva, Long>
		implements ReservaDao {

}
