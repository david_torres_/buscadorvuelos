package es.udc.med.team04.buscadorvuelos.model.reservaservice;

import es.udc.med.team04.buscadorvuelos.model.reserva.Reserva;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;

public interface ReservaService {

	public Reserva reservar(Long ida, Long vuelta, String nombre,
			String apellidos, String dni, String telefono, String clase,
			String ubicacion) throws InstanceNotFoundException;

	public boolean cancelarReserva(Long reservaId, String dni)
			throws InstanceNotFoundException;
}
