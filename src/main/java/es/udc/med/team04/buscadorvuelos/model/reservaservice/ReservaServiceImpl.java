package es.udc.med.team04.buscadorvuelos.model.reservaservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.med.team04.buscadorvuelos.model.reserva.Reserva;
import es.udc.med.team04.buscadorvuelos.model.reserva.ReservaDao;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;
import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;
import es.udc.med.team04.buscadorvuelos.model.vuelo.VueloDao;

@Service("ReservaService")
@Transactional
public class ReservaServiceImpl implements ReservaService {

	@Autowired
	private ReservaDao reservaDao;

	@Autowired
	private VueloDao vueloDao;

	@Override
	public Reserva reservar(Long ida, Long vuelta, String nombre,
			String apellidos, String dni, String telefono, String clase,
			String ubicacion) throws InstanceNotFoundException {
		Float precio;
		Vuelo vuelo = vueloDao.find(ida);

		int tarifaIda = 0;
		int tarifaVuelta = 0;
		String ubicacion2 = null;
		String clase2 = null;

		if (clase.compareTo("Normal") == 0) {
			ubicacion = auxNormal(vuelo, vuelta, ubicacion);
			if (vuelta != null) {
				clase2 = clase;
			}
		} else if (clase.compareTo("Vip") == 0) {
			ubicacion = auxVip(vuelo, vuelta, ubicacion);
			tarifaIda = 1;
			tarifaVuelta = 1;
			if (vuelta != null) {
				clase2 = clase;
			}
		} else {
			if (ubicacion.compareTo("pasillo") == 0) {
				if (vuelo.getNumPlazasLibresNormalPas() > 0) {
					vuelo.setNumPlazasLibresNormalPas(vuelo
							.getNumPlazasLibresNormalPas() - 1);
					clase = "normal";
				} else {
					vuelo.setNumPlazasLibresVipPas(vuelo
							.getNumPlazasLibresVipPas() - 1);
					tarifaIda = 1;
					clase = "vip";
				}
				if (vuelta != null) {
					Vuelo vueloVuelta = vueloDao.find(vuelta);
					if (vueloVuelta.getNumPlazasLibresNormalPas() > 0) {
						vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
								.getNumPlazasLibresNormalPas() - 1);
						clase2 = "normal";
					} else {
						vueloVuelta.setNumPlazasLibresVipPas(vueloVuelta
								.getNumPlazasLibresVipPas() - 1);
						tarifaVuelta = 1;
						clase2 = "vip";
					}
				}
			} else if (ubicacion.compareTo("ventana") == 0) {
				if (vuelo.getNumPlazasLibresNormalVent() > 0) {
					vuelo.setNumPlazasLibresNormalVent(vuelo
							.getNumPlazasLibresNormalVent() - 1);
					clase = "normal";
				} else {
					vuelo.setNumPlazasLibresVipVent(vuelo
							.getNumPlazasLibresVipVent() - 1);
					tarifaIda = 1;
					clase = "vip";
				}
				if (vuelta != null) {
					Vuelo vueloVuelta = vueloDao.find(vuelta);
					if (vueloVuelta.getNumPlazasLibresNormalVent() > 0) {
						vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
								.getNumPlazasLibresNormalVent() - 1);
						clase2 = "normal";
					} else {
						vueloVuelta.setNumPlazasLibresVipVent(vueloVuelta
								.getNumPlazasLibresVipVent() - 1);
						tarifaVuelta = 1;
						clase2 = "vip";
					}
				}
			} else {
				if (vuelo.getNumPlazasLibresNormalPas() > 0) {
					vuelo.setNumPlazasLibresNormalPas(vuelo
							.getNumPlazasLibresNormalPas() - 1);
					clase = "normal";
					ubicacion = "pasillo";
				} else if (vuelo.getNumPlazasLibresNormalVent() > 0) {
					vuelo.setNumPlazasLibresNormalVent(vuelo
							.getNumPlazasLibresNormalVent() - 1);
					clase = "normal";
					ubicacion = "ventana";
				} else if (vuelo.getNumPlazasLibresVipPas() > 0) {
					vuelo.setNumPlazasLibresVipPas(vuelo
							.getNumPlazasLibresVipPas() - 1);
					tarifaIda = 1;
					clase = "vip";
					ubicacion = "pasillo";
				} else {
					vuelo.setNumPlazasLibresVipVent(vuelo
							.getNumPlazasLibresVipVent() - 1);
					tarifaIda = 1;
					clase = "vip";
					ubicacion = "ventana";
				}
				if (vuelta != null) {
					Vuelo vueloVuelta = vueloDao.find(vuelta);
					if (vueloVuelta.getNumPlazasLibresNormalPas() > 0) {
						vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
								.getNumPlazasLibresNormalPas() - 1);
						clase2 = "normal";
						ubicacion2 = "pasillo";
					} else if (vueloVuelta.getNumPlazasLibresNormalVent() > 0) {
						vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
								.getNumPlazasLibresNormalVent() - 1);
						clase2 = "normal";
						ubicacion2 = "ventana";
					} else if (vueloVuelta.getNumPlazasLibresVipPas() > 0) {
						vueloVuelta.setNumPlazasLibresVipPas(vueloVuelta
								.getNumPlazasLibresVipPas() - 1);
						tarifaVuelta = 1;
						clase2 = "vip";
						ubicacion2 = "pasillo";
					} else {
						vueloVuelta.setNumPlazasLibresVipVent(vueloVuelta
								.getNumPlazasLibresVipVent() - 1);
						tarifaVuelta = 1;
						clase2 = "vip";
						ubicacion2 = "ventana";
					}
				}
			}
		}
		if (vuelta != null) {
			Vuelo vueloVuelta = vueloDao.find(vuelta);
			precio = calcularPrecio(vuelo.getPrecioBase(),
					vueloVuelta.getPrecioBase(), tarifaIda, tarifaVuelta);
			if (ubicacion.compareTo("pasillo,ventana") == 0) {
				ubicacion = "pasillo";
				ubicacion2 = "ventana";
			} else if (ubicacion.compareTo("ventana,pasillo") == 0) {
				ubicacion = "ventana";
				ubicacion2 = "pasillo";
			} else {
				ubicacion2 = ubicacion;
			}
			Reserva reserva = new Reserva(vuelo, vueloVuelta, nombre,
					apellidos, dni, telefono, clase, clase2, ubicacion,
					ubicacion2, precio);
			reservaDao.save(reserva);
			return reserva;
		} else {
			precio = calcularPrecio(vuelo.getPrecioBase(), null, tarifaIda,
					tarifaVuelta);
			Reserva reserva = new Reserva(vuelo, null, nombre, apellidos, dni,
					telefono, clase, clase2, ubicacion, ubicacion2, precio);
			reservaDao.save(reserva);
			return reserva;
		}

	}

	private Float calcularPrecio(Float precioBaseIda, Float precioBaseVuelta,
			int tarifaIda, int tarifaVuelta) {
		Float resultado;
		if (tarifaIda == 0) {
			resultado = (float) (precioBaseIda * 1.2);
		} else {
			resultado = (float) (precioBaseVuelta * 1.5);
		}

		if (precioBaseVuelta != null) {
			if (tarifaIda == 0) {
				resultado += (float) (precioBaseVuelta * 1.2);
			} else {
				resultado += (float) (precioBaseVuelta * 1.5);
			}
		}

		return resultado;

	}

	private String auxNormal(Vuelo vuelo, Long vuelta, String ubicacion)
			throws InstanceNotFoundException {

		if (ubicacion.compareTo("pasillo") == 0) {
			if (vuelta != null) {
				Vuelo vueloVuelta = vueloDao.find(vuelta);
				vuelo.setNumPlazasLibresNormalPas(vuelo
						.getNumPlazasLibresNormalPas() - 1);
				vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
						.getNumPlazasLibresNormalPas() - 1);
			} else {
				vuelo.setNumPlazasLibresNormalPas(vuelo
						.getNumPlazasLibresNormalPas() - 1);
			}
			return ubicacion;
		} else if (ubicacion.compareTo("ventana") == 0) {
			if (vuelta != null) {
				Vuelo vueloVuelta = vueloDao.find(vuelta);
				vuelo.setNumPlazasLibresNormalVent(vuelo
						.getNumPlazasLibresNormalVent() - 1);
				vueloVuelta.setNumPlazasLibresNormalVent(vueloVuelta
						.getNumPlazasLibresNormalVent() - 1);
			} else {
				vuelo.setNumPlazasLibresNormalVent(vuelo
						.getNumPlazasLibresNormalVent() - 1);
			}
			return ubicacion;
		} else {
			if (vuelta != null) {
				Vuelo vueloVuelta = vueloDao.find(vuelta);
				if (vuelo.getNumPlazasLibresNormalPas() > 0) {
					vuelo.setNumPlazasLibresNormalPas(vuelo
							.getNumPlazasLibresNormalPas() - 1);
					if (vueloVuelta.getNumPlazasLibresNormalPas() > 0) {
						vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
								.getNumPlazasLibresNormalPas() - 1);
						return "pasillo";
					} else {
						vueloVuelta.setNumPlazasLibresNormalVent(vuelo
								.getNumPlazasLibresNormalVent() - 1);
						return "pasillo,ventana";
					}
				} else {
					vuelo.setNumPlazasLibresNormalVent(vuelo
							.getNumPlazasLibresNormalVent() - 1);
					if (vueloVuelta.getNumPlazasLibresNormalPas() > 0) {
						vueloVuelta.setNumPlazasLibresNormalPas(vueloVuelta
								.getNumPlazasLibresNormalPas() - 1);
						return "ventana,pasillo";
					} else {
						vueloVuelta.setNumPlazasLibresNormalVent(vuelo
								.getNumPlazasLibresNormalVent() - 1);
						return "ventana";
					}
				}
			} else {
				if (vuelo.getNumPlazasLibresNormalPas() > 0) {
					vuelo.setNumPlazasLibresNormalPas(vuelo
							.getNumPlazasLibresNormalPas() - 1);
					return "pasillo";
				} else {
					vuelo.setNumPlazasLibresNormalVent(vuelo
							.getNumPlazasLibresNormalVent() - 1);
					return "ventana";
				}
			}
		}
	}

	private String auxVip(Vuelo vuelo, Long vuelta, String ubicacion)
			throws InstanceNotFoundException {

		if (ubicacion.compareTo("pasillo") == 0) {
			if (vuelta != null) {
				Vuelo vueloVuelta = vueloDao.find(vuelta);
				vuelo.setNumPlazasLibresVipPas(vuelo.getNumPlazasLibresVipPas() - 1);
				vueloVuelta.setNumPlazasLibresVipPas(vueloVuelta
						.getNumPlazasLibresVipPas() - 1);
			} else {
				vuelo.setNumPlazasLibresVipPas(vuelo.getNumPlazasLibresVipPas() - 1);
			}
			return ubicacion;
		} else if (ubicacion.compareTo("ventana") == 0) {
			if (vuelta != null) {
				Vuelo vueloVuelta = vueloDao.find(vuelta);
				vuelo.setNumPlazasLibresVipVent(vuelo
						.getNumPlazasLibresVipVent() - 1);
				vueloVuelta.setNumPlazasLibresVipVent(vueloVuelta
						.getNumPlazasLibresVipVent() - 1);
			} else {
				vuelo.setNumPlazasLibresVipVent(vuelo
						.getNumPlazasLibresVipVent() - 1);
			}
			return ubicacion;
		} else {
			if (vuelta != null) {
				Vuelo vueloVuelta = vueloDao.find(vuelta);
				if (vuelo.getNumPlazasLibresVipPas() > 0) {
					vuelo.setNumPlazasLibresVipPas(vuelo
							.getNumPlazasLibresVipPas() - 1);
					if (vueloVuelta.getNumPlazasLibresVipPas() > 0) {
						vueloVuelta.setNumPlazasLibresVipPas(vueloVuelta
								.getNumPlazasLibresVipPas() - 1);
						return "pasillo";
					} else {
						vueloVuelta.setNumPlazasLibresVipVent(vuelo
								.getNumPlazasLibresVipVent() - 1);
						return "pasillo,ventana";
					}
				} else {
					vuelo.setNumPlazasLibresVipVent(vuelo
							.getNumPlazasLibresVipVent() - 1);
					if (vueloVuelta.getNumPlazasLibresVipPas() > 0) {
						vueloVuelta.setNumPlazasLibresVipPas(vueloVuelta
								.getNumPlazasLibresVipPas() - 1);
						return "ventana,pasillo";
					} else {
						vueloVuelta.setNumPlazasLibresVipVent(vuelo
								.getNumPlazasLibresVipVent() - 1);
						return "ventana";
					}
				}
			} else {
				if (vuelo.getNumPlazasLibresVipPas() > 0) {
					vuelo.setNumPlazasLibresVipPas(vuelo
							.getNumPlazasLibresVipPas() - 1);
					return "pasillo";
				} else {
					vuelo.setNumPlazasLibresVipVent(vuelo
							.getNumPlazasLibresVipVent() - 1);
					return "ventana";
				}
			}
		}

	}

	@Override
	public boolean cancelarReserva(Long reservaId, String dni)
			throws InstanceNotFoundException {

		Reserva reserva = reservaDao.find(reservaId);
		Vuelo ida = vueloDao.find(reserva.getIda().getVueloId());
		if (!reserva.getDni().equals(dni)) {
			return false;
		}

		if ((reserva.getClase().equals("vip"))
				&& (reserva.getUbicacion().equals("pasillo"))) {
			ida.setNumPlazasLibresVipPas(reserva.getIda()
					.getNumPlazasLibresVipPas() + 1);
		}

		if ((reserva.getClase().equals("vip"))
				&& (reserva.getUbicacion().equals("ventana"))) {
			ida.setNumPlazasLibresVipVent(reserva.getIda()
					.getNumPlazasLibresVipVent() + 1);
		}

		if ((reserva.getClase().equals("normal"))
				&& (reserva.getUbicacion().equals("pasillo"))) {
			ida.setNumPlazasLibresNormalPas(reserva.getIda()
					.getNumPlazasLibresNormalPas() + 1);
		}

		if ((reserva.getClase().equals("normal"))
				&& (reserva.getUbicacion().equals("ventana"))) {
			ida.setNumPlazasLibresNormalVent(reserva.getIda()
					.getNumPlazasLibresNormalVent() + 1);
		}

		vueloDao.save(ida);

		if (reserva.getVuelta() != null) {
			Vuelo vuelta = vueloDao.find(reserva.getVuelta().getVueloId());

			if ((reserva.getClase().equals("vip"))
					&& (reserva.getUbicacion().equals("pasillo"))) {
				vuelta.setNumPlazasLibresVipPas(reserva.getVuelta()
						.getNumPlazasLibresVipPas() + 1);
			}

			if ((reserva.getClase().equals("vip"))
					&& (reserva.getUbicacion().equals("ventana"))) {
				vuelta.setNumPlazasLibresVipVent(reserva.getVuelta()
						.getNumPlazasLibresVipVent() + 1);
			}

			if ((reserva.getClase().equals("normal"))
					&& (reserva.getUbicacion().equals("pasillo"))) {
				vuelta.setNumPlazasLibresNormalPas(reserva.getVuelta()
						.getNumPlazasLibresNormalPas() + 1);
			}

			if ((reserva.getClase().equals("normal"))
					&& (reserva.getUbicacion().equals("ventana"))) {
				vuelta.setNumPlazasLibresNormalVent(reserva.getVuelta()
						.getNumPlazasLibresNormalVent() + 1);
			}

			vueloDao.save(vuelta);
		}
		try {
			reservaDao.remove(reservaId);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
