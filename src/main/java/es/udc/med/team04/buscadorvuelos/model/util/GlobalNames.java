package es.udc.med.team04.buscadorvuelos.model.util;

public class GlobalNames {

	public static final String SPRING_CONFIG_FILE = "classpath:/spring-config.xml";

	private GlobalNames() {
	}

}
