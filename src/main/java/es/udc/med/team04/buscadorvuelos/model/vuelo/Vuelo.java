package es.udc.med.team04.buscadorvuelos.model.vuelo;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Proxy;

import es.udc.med.team04.buscadorvuelos.model.aeropuerto.Aeropuerto;
import es.udc.med.team04.buscadorvuelos.model.avion.Avion;

@Entity
@Proxy(lazy = false)
public class Vuelo {

	private Long vueloId;
	private Aeropuerto origen;
	private Aeropuerto destino;
	private Avion avion;
	private Calendar fechaSalida;
	private Float precioBase;
	private int numPlazasLibresNormalVent;
	private int numPlazasLibresNormalPas;
	private int numPlazasLibresVipVent;
	private int numPlazasLibresVipPas;
	private String info;

	public Vuelo() {

	}

	public Vuelo(Aeropuerto origen, Aeropuerto destino, Avion avion,
			Calendar fechaSalida, Float precioBase, String info) {

		this.origen = origen;
		this.destino = destino;
		this.avion = avion;
		this.fechaSalida = fechaSalida;
		this.precioBase = precioBase;
		this.numPlazasLibresNormalVent = avion.getNumPlazasNormalVent();
		this.numPlazasLibresNormalPas = avion.getNumPlazasNormalPas();
		this.numPlazasLibresVipVent = avion.getNumPlazasVipVent();
		this.numPlazasLibresVipPas = avion.getNumPlazasVipPas();
		this.info = info;
	}

	@Column(name = "vueloId")
	@SequenceGenerator( // It only takes effect for
	name = "VueloIdGenerator", // databases providing identifier
	sequenceName = "VueloSeq")
	// generators.
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "VueloIdGenerator")
	public Long getVueloId() {
		return vueloId;
	}

	public void setVueloId(Long vueloId) {
		this.vueloId = vueloId;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "origen")
	public Aeropuerto getOrigen() {
		return origen;
	}

	public void setOrigen(Aeropuerto origen) {
		this.origen = origen;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destino")
	public Aeropuerto getDestino() {
		return destino;
	}

	public void setDestino(Aeropuerto destino) {
		this.destino = destino;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "avion")
	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	@Temporal(TemporalType.DATE)
	public Calendar getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(Calendar fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public Float getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(Float precioBase) {
		this.precioBase = precioBase;
	}

	public int getNumPlazasLibresNormalVent() {
		return numPlazasLibresNormalVent;
	}

	public void setNumPlazasLibresNormalVent(int plazasLibresNormalVent) {
		this.numPlazasLibresNormalVent = plazasLibresNormalVent;
	}

	public int getNumPlazasLibresNormalPas() {
		return numPlazasLibresNormalPas;
	}

	public void setNumPlazasLibresNormalPas(int plazasLibresNormalPas) {
		this.numPlazasLibresNormalPas = plazasLibresNormalPas;
	}

	public int getNumPlazasLibresVipVent() {
		return numPlazasLibresVipVent;
	}

	public void setNumPlazasLibresVipVent(int plazasLibresVipVent) {
		this.numPlazasLibresVipVent = plazasLibresVipVent;
	}

	public int getNumPlazasLibresVipPas() {
		return numPlazasLibresVipPas;
	}

	public void setNumPlazasLibresVipPas(int plazasLibresVipPas) {
		this.numPlazasLibresVipPas = plazasLibresVipPas;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}
