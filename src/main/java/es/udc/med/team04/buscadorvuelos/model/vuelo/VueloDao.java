package es.udc.med.team04.buscadorvuelos.model.vuelo;

import java.util.Calendar;
import java.util.List;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDao;


public interface VueloDao extends GenericDao<Vuelo, Long> {

	public List<Vuelo> findVuelos(Long origen, Long destino, Calendar fecha,
			String clase, String plaza, int startIndex, int count);

}
