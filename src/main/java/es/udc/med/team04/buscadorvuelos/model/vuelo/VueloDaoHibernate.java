package es.udc.med.team04.buscadorvuelos.model.vuelo;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.med.team04.buscadorvuelos.model.util.GenericDaoHibernate;


@Repository("VueloDao")
public class VueloDaoHibernate extends GenericDaoHibernate<Vuelo, Long>
		implements VueloDao {

	@SuppressWarnings("unchecked")
	private List<Vuelo> findVuelosDisponibles(Long origen, Long destino,
			Calendar fechaSalida, int startIndex, int count) {
		return getSession()
				.createQuery(
						"SELECT u FROM Vuelo u WHERE u.destino.aeroId = :destino"
								+ " AND u.origen.aeroId = :origen"
								+ " AND u.fechaSalida = :fechaSalida"
								+ " AND (u.numPlazasLibresNormalVent"
								+ " + u.numPlazasLibresNormalPas"
								+ " + u.numPlazasLibresVipVent"
								+ " + u.numPlazasLibresVipPas) > 0"
								+ " ORDER BY u.vueloId")
				.setParameter("destino", destino)
				.setParameter("origen", origen)
				.setParameter("fechaSalida", fechaSalida)
				.setFirstResult(startIndex).setMaxResults(count).list();
	}

	@SuppressWarnings("unchecked")
	private List<Vuelo> findVuelosClase(Long origen, Long destino,
			Calendar fechaSalida, String clase, int startIndex, int count) {

		String query = "SELECT u FROM Vuelo u WHERE u.destino.aeroId = :destino"
				+ " AND u.origen.aeroId = :origen"
				+ " AND u.fechaSalida = :fechaSalida";

		if (clase.equals("vip")) {
			query = query
					.concat(" AND u.numPlazasLibresVipVent + u.numPlazasLibresVipPas >0");
		} else {
			query = query
					.concat(" AND u.numPlazasLibresNormalVent + u.numPlazasLibresNormalPas >0");
		}

		query = query.concat(" ORDER BY u.vueloId");

		return getSession().createQuery(query).setParameter("destino", destino)
				.setParameter("origen", origen)
				.setParameter("fechaSalida", fechaSalida)
				.setFirstResult(startIndex).setMaxResults(count).list();
	}

	@SuppressWarnings("unchecked")
	private List<Vuelo> findVuelosPlaza(Long origen, Long destino,
			Calendar fechaSalida, String plaza, int startIndex, int count) {

		String query = "SELECT u FROM Vuelo u WHERE u.destino.aeroId = :destino"
				+ " AND u.origen.aeroId = :origen"
				+ " AND u.fechaSalida = :fechaSalida";

		if (plaza.equals("ventana")) {
			query = query
					.concat(" AND u.numPlazasLibresVipVent + u.numPlazasLibresNormalVent >0");
		} else {
			query = query
					.concat(" AND u.numPlazasLibresVipPas + u.numPlazasLibresNormalPas >0");
		}

		query = query.concat(" ORDER BY u.vueloId DESC");

		return getSession().createQuery(query).setParameter("destino", destino)
				.setParameter("origen", origen)
				.setParameter("fechaSalida", fechaSalida)
				.setFirstResult(startIndex).setMaxResults(count).list();
	}

	@SuppressWarnings("unchecked")
	private List<Vuelo> findVuelosClasePlaza(Long origen, Long destino,
			Calendar fechaSalida, String clase, String plaza, int startIndex,
			int count) {
		String query = "SELECT u FROM Vuelo u WHERE u.destino.aeroId = :destino"
				+ " AND u.origen.aeroId = :origen"
				+ " AND u.fechaSalida = :fechaSalida";

		if ((clase.equals("vip")) && (plaza.equals("pasillo"))) {
			query = query.concat(" AND numPlazasLibresVipPas >0");
		}

		if ((clase.equals("vip")) && (plaza.equals("ventana"))) {
			query = query.concat(" AND numPlazasLibresVipVent >0");
		}

		if ((clase.equals("turista")) && (plaza.equals("ventana"))) {
			query = query.concat(" AND numPlazasLibresNormalVent >0");
		}
		if ((clase.equals("turista")) && (plaza.equals("pasillo"))) {
			query = query.concat(" AND numPlazasLibresNormalPas >0");
		}

		query = query.concat(" ORDER BY u.vueloId");

		return getSession().createQuery(query).setParameter("destino", destino)
				.setParameter("origen", origen)
				.setParameter("fechaSalida", fechaSalida)
				.setFirstResult(startIndex).setMaxResults(count).list();
	}

	@Override
	public List<Vuelo> findVuelos(Long origen, Long destino, Calendar fecha,
			String clase, String plaza, int startIndex, int count) {

		if ((clase.isEmpty()) && (plaza.isEmpty())) {/*
													 * Caso Todos los vuelos
													 * disponibles
													 */
			return findVuelosDisponibles(origen, destino, fecha, startIndex,
					count);
		}

		if ((!clase.isEmpty()) && (plaza.isEmpty())) {/* Caso Busqueda por Clase */
			return findVuelosClase(origen, destino, fecha, clase, startIndex,
					count);
		}

		if ((clase.isEmpty()) && (!plaza.isEmpty())) {/* Caso Busqueda por plaza */
			return findVuelosPlaza(origen, destino, fecha, plaza, startIndex,
					count);
		}
		/* Caso Busqueda por plaza y clase */
		return findVuelosClasePlaza(origen, destino, fecha, clase, plaza,
				startIndex, count);
	}

}
