package es.udc.med.team04.buscadorvuelos.model.vueloservice;

import java.util.Calendar;
import java.util.List;

import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;
import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;

public interface VueloService {

	public List<Vuelo> buscarVuelos(Long origen, Long destino,
			Calendar fechaIda, Calendar fechaVuelta, String clase,
			String plaza, int startIndex, int count);

	public Vuelo findById(Long vueloId) throws InstanceNotFoundException;
}
