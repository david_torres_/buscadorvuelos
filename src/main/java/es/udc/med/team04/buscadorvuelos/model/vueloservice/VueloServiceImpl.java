package es.udc.med.team04.buscadorvuelos.model.vueloservice;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;
import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;
import es.udc.med.team04.buscadorvuelos.model.vuelo.VueloDao;

@Service("VueloService")
@Transactional
public class VueloServiceImpl implements VueloService {

	@Autowired
	private VueloDao vueloDao;

	@Override
	public List<Vuelo> buscarVuelos(Long origenId, Long destinoId,
			Calendar fechaIda, Calendar Vuelta, String clase, String plaza,
			int startIndex, int count) {
		if (Vuelta == null) {
			return vueloDao.findVuelos(origenId, destinoId, fechaIda, clase,
					plaza, startIndex, count);

		} else {
			List<Vuelo> idas = vueloDao.findVuelos(origenId, destinoId,
					fechaIda, clase, plaza, startIndex, count);

			List<Vuelo> vueltas = vueloDao.findVuelos(destinoId, origenId,
					Vuelta, clase, plaza, startIndex, count);
			if ((!idas.isEmpty()) && (!vueltas.isEmpty())) {
				idas.addAll(vueltas);/*
									 * Revisar!!!! no devuelve ningun vuelo si
									 * no hay una vuelta o una ida
									 */
				return idas;
			} else {

				idas.clear();
				return idas;
			}
		}
	}

	@Override
	public Vuelo findById(Long vueloId) throws InstanceNotFoundException {
		return vueloDao.find(vueloId);
	}
}
