-- Indexes for primary keys have been explicitly created.
 
-- ---------- Table for validation queries from the connection pool. ----------
 
DROP TABLE PingTable;
CREATE TABLE PingTable (foo CHAR(1));
 
-- ------------------------------ UserProfile ----------------------------------

DROP TABLE Reserva; 
DROP TABLE Vuelo;
DROP TABLE Aeropuerto;
DROP TABLE Avion;
 
CREATE TABLE Aeropuerto (
    aeroId BIGINT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(30) COLLATE latin1_bin NOT NULL,
    direccion VARCHAR(60) NOT NULL,
    pais VARCHAR(40) NOT NULL,
    nombreCorto VARCHAR(10) NOT NULL,
    CONSTRAINT Aeropuerto_PK PRIMARY KEY (aeroId),
    CONSTRAINT NombreUniqueKey UNIQUE (nombre));
 
CREATE INDEX AeropuertoIndexByNombre ON Aeropuerto (nombreCorto);
 
CREATE TABLE Avion (
    avionId BIGINT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    numPlazasNormalPas INT,
    numPlazasNormalVent INT,
    numPlazasVipPas INT,
    numPlazasVipVent INT,
    CONSTRAINT Avion_PK PRIMARY KEY (avionId));
    
CREATE TABLE Vuelo (
    vueloId BIGINT NOT NULL AUTO_INCREMENT,
    origen BIGINT,
    destino BIGINT,
    avion BIGINT,
    fechaSalida DATE,
    precioBase FLOAT NOT NULL,
    numPlazasLibresNormalPas INT,
    numPlazasLibresNormalVent INT,
    numPlazasLibresVipPas INT,
    numPlazasLibresVipVent INT,
    info VARCHAR(100),
    CONSTRAINT Vuelo_PK PRIMARY KEY (vueloId),
    CONSTRAINT Vuelo_OrigenFK FOREIGN KEY (origen) REFERENCES Aeropuerto(aeroId),
    CONSTRAINT Vuelo_DestinoFK FOREIGN KEY (destino) REFERENCES Aeropuerto(aeroId),
    CONSTRAINT Vuelo_AvionFK FOREIGN KEY (avion) REFERENCES Avion(avionId));
    
CREATE INDEX VueloIndexByOrigen ON Vuelo (origen);
CREATE INDEX VueloIndexByDestino ON Vuelo (destino);
CREATE INDEX VueloIndexByFecha ON Vuelo (fechaSalida);
   
CREATE TABLE Reserva (
    reservaId BIGINT NOT NULL AUTO_INCREMENT,
    ida BIGINT,
    vuelta BIGINT,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    dni VARCHAR(9) NOT NULL,
    telefono VARCHAR(12) NOT NULL,
    clase VARCHAR(50) NOT NULL,
    clase2 VARCHAR(50),
    ubicacion VARCHAR(50) NOT NULL,
    ubicacion2 VARCHAR(50),
    precioBase FLOAT NOT NULL,
    CONSTRAINT Reserva_PK PRIMARY KEY (reservaId),
    CONSTRAINT Reserva_VueloIda_FK FOREIGN KEY (ida) REFERENCES Vuelo(vueloId),
    CONSTRAINT Reserva_VueloVuelta_FK FOREIGN KEY (vuelta) REFERENCES Vuelo(vueloId));
