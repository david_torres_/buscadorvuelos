-- INSERT INTO Aeropuerto VALUES(aeroId, nombre, direccion, pais, nombreCorto); 

INSERT INTO Aeropuerto VALUES(1, "aeropuertito", "algun lugar", "Españistan", "tito"); 
INSERT INTO Aeropuerto VALUES(2, "aeropuertito 2", "algun lugar 2", "Españistan", "tito 2");

-- INSERT INTO Avion (avionId,nombre,numPlazasNormalPas, numPlazasNormalVent, numPlazasVipPas, numPlazasVipVent);

INSERT INTO Avion VALUES (1,"avioncito",5, 5, 5, 5);
INSERT INTO Avion VALUES (2,"avion",10, 10, 10, 10);

-- INSERT INTO Vuelo ( vueloId, origen, destino, avion, fechaSalida, precioBase, numPlazasLibresNormalPas, NumPlazasLibresNormalVent, numPlazasLibresVipPaS, numPlazasLibresVipVent, info);

INSERT INTO Vuelo VALUES( 1, 1, 2, 1, "2015/11/02", 50, 5, 5, 5, 5, "cosas");	
