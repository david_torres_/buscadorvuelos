package es.udc.med.team04.buscadorvuelos.test.reservaservice;

import static es.udc.med.team04.buscadorvuelos.model.util.GlobalNames.SPRING_CONFIG_FILE;
import static es.udc.med.team04.buscadorvuelos.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.med.team04.buscadorvuelos.model.aeropuerto.Aeropuerto;
import es.udc.med.team04.buscadorvuelos.model.aeropuerto.AeropuertoDao;
import es.udc.med.team04.buscadorvuelos.model.avion.Avion;
import es.udc.med.team04.buscadorvuelos.model.avion.AvionDao;
import es.udc.med.team04.buscadorvuelos.model.reserva.Reserva;
import es.udc.med.team04.buscadorvuelos.model.reserva.ReservaDao;
import es.udc.med.team04.buscadorvuelos.model.reservaservice.ReservaService;
import es.udc.med.team04.buscadorvuelos.model.util.InstanceNotFoundException;
import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;
import es.udc.med.team04.buscadorvuelos.model.vuelo.VueloDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class ReservaServiceTest {

	@Autowired
	private ReservaDao reservaDao;

	@Autowired
	private VueloDao vueloDao;

	@Autowired
	private AeropuertoDao aeropuertoDao;

	@Autowired
	private AvionDao avionDao;

	@Autowired
	private ReservaService reservaService;

	/********************************* Test*historia*H6 *****************************************************/

	@Test
	public void ReservarVuelo() throws InstanceNotFoundException {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");

		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);

		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");

		vueloDao.save(vuelo);

		Reserva reserva = reservaService.reservar(vuelo.getVueloId(), null,
				"Manuel", "Deaquipalla", "65467516F", "667612847", "", "");

		assertEquals(vuelo.getVueloId(), reserva.getIda().getVueloId());
		assertEquals(
				99,
				vuelo.getNumPlazasLibresNormalPas()
						+ vuelo.getNumPlazasLibresNormalVent()
						+ vuelo.getNumPlazasLibresVipPas()
						+ vuelo.getNumPlazasLibresVipVent());

	}

	/********************************* Test*historia*H5 *****************************************************/
	@Test
	public void cancelarReserva() throws InstanceNotFoundException {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");

		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);

		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");

		vueloDao.save(vuelo);

		Reserva reserva = reservaService.reservar(vuelo.getVueloId(), null,
				"Manuel", "Deaquipalla", "65467516F", "667612847", "vip",
				"pasillo");
		reservaDao.save(reserva);

		assertEquals(
				99,
				vuelo.getNumPlazasLibresNormalPas()
						+ vuelo.getNumPlazasLibresNormalVent()
						+ vuelo.getNumPlazasLibresVipPas()
						+ vuelo.getNumPlazasLibresVipVent());

		boolean reply = reservaService.cancelarReserva(reserva.getReservaId(),
				reserva.getDni());

		assertTrue(reply);
		assertEquals(
				100,
				vuelo.getNumPlazasLibresNormalPas()
						+ vuelo.getNumPlazasLibresNormalVent()
						+ vuelo.getNumPlazasLibresVipPas()
						+ vuelo.getNumPlazasLibresVipVent());
	}

	@Test
	public void CancelarReservaIdaYVuelta() throws InstanceNotFoundException {

		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);

		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);
		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 5, 6);

		Calendar fechaVuelta = Calendar.getInstance();
		fechaVuelta.set(2015, 6, 8);

		Vuelo ida = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");
		vueloDao.save(ida);

		Vuelo vuelta = new Vuelo(destino, salida, avion, fechaVuelta,
				new Float(230), "");
		vueloDao.save(vuelta);

		Reserva reserva = reservaService.reservar(ida.getVueloId(),
				vuelta.getVueloId(), "Manuel", "Deaquipalla", "65467516F",
				"667612847", "", "");
		reservaDao.save(reserva);
		assertEquals(
				99,
				ida.getNumPlazasLibresNormalPas()
						+ ida.getNumPlazasLibresNormalVent()
						+ ida.getNumPlazasLibresVipPas()
						+ ida.getNumPlazasLibresVipVent());
		assertEquals(
				99,
				vuelta.getNumPlazasLibresNormalPas()
						+ vuelta.getNumPlazasLibresNormalVent()
						+ vuelta.getNumPlazasLibresVipPas()
						+ vuelta.getNumPlazasLibresVipVent());

		boolean reply = reservaService.cancelarReserva(reserva.getReservaId(),
				reserva.getDni());

		assertTrue(reply);
		assertEquals(
				100,
				ida.getNumPlazasLibresNormalPas()
						+ ida.getNumPlazasLibresNormalVent()
						+ ida.getNumPlazasLibresVipPas()
						+ ida.getNumPlazasLibresVipVent());
		assertEquals(
				100,
				vuelta.getNumPlazasLibresNormalPas()
						+ vuelta.getNumPlazasLibresNormalVent()
						+ vuelta.getNumPlazasLibresVipPas()
						+ vuelta.getNumPlazasLibresVipVent());
	}

	@Test(expected = InstanceNotFoundException.class)
	public void CancelarReservaInexistente() throws InstanceNotFoundException {

		reservaService.cancelarReserva(new Long(666), "23589334G");
	}
}
