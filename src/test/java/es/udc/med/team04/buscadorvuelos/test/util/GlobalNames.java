package es.udc.med.team04.buscadorvuelos.test.util;

public final class GlobalNames {

	public static final String SPRING_CONFIG_TEST_FILE = "classpath:/spring-config-test.xml";

	private GlobalNames() {
	}

}
