package es.udc.med.team04.buscadorvuelos.test.vueloservice;

import static es.udc.med.team04.buscadorvuelos.model.util.GlobalNames.SPRING_CONFIG_FILE;
import static es.udc.med.team04.buscadorvuelos.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.med.team04.buscadorvuelos.model.aeropuerto.Aeropuerto;
import es.udc.med.team04.buscadorvuelos.model.aeropuerto.AeropuertoDao;
import es.udc.med.team04.buscadorvuelos.model.avion.Avion;
import es.udc.med.team04.buscadorvuelos.model.avion.AvionDao;
import es.udc.med.team04.buscadorvuelos.model.vuelo.Vuelo;
import es.udc.med.team04.buscadorvuelos.model.vuelo.VueloDao;
import es.udc.med.team04.buscadorvuelos.model.vueloservice.VueloService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class VueloServiceTest {

	@Autowired
	private VueloDao vueloDao;

	@Autowired
	private AeropuertoDao aeropuertoDao;

	@Autowired
	private AvionDao avionDao;

	@Autowired
	private VueloService buscador;

	/********************************* Test*historia*H1 ****************************************************/
	@Test
	public void BuscarVueloExistente() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");

		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);

		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");

		vueloDao.save(vuelo);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "", "", 0, 5);

		assertEquals(1, vuelos.size());
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());
	}

	@Test
	public void BuscarVueloVacio() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);
		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");

		vueloDao.save(vuelo);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				salida.getAeroId(), fechaSalida, null, "", "", 0, 5);
		assertEquals(vuelos.size(), 0);
	}

	@Test
	public void BuscarVueloFecha() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);
		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Calendar fechaSalida2 = Calendar.getInstance();
		fechaSalida2.set(2016, 1, 12);

		Vuelo vuelo = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");

		vueloDao.save(vuelo);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida2, null, "", "", 0, 5);
		assertEquals(vuelos.size(), 0);
	}

	@Test
	public void BuscarVueloSinPlazas() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 0, 0);
		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo = new Vuelo(salida, destino, avion, fechaSalida, new Float(
				200), "");

		vueloDao.save(vuelo);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "", "", 0, 5);
		assertEquals(vuelos.size(), 0);
	}

	@Test
	public void BuscarVarios() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avion = new Avion("avioncito", 50, 50);
		avionDao.save(avion);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avion, fechaSalida,
				new Float(500), "Comida incluida");

		Vuelo vuelo2 = new Vuelo(salida, destino, avion, fechaSalida,
				new Float(200), "");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "", "", 0, 5);

		assertEquals(vuelos.size(), 2);
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());

		assertEquals(salida.getNombreCorto(), vuelos.get(1).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(1).getDestino()
				.getNombreCorto());

		assertEquals(vuelo1.getPrecioBase(), vuelos.get(0).getPrecioBase());
		assertEquals(vuelo2.getPrecioBase(), vuelos.get(1).getPrecioBase());
	}

	/********************************* Test*historia*H2 ****************************************************/
	@Test
	public void BuscarVueloClaseExistente() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionVip = new Avion("avioncito", 0, 50);
		avionDao.save(avionVip);

		Avion avionTurista = new Avion("avioncito2", 50, 0);
		avionDao.save(avionTurista);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avionVip, fechaSalida,
				new Float(200), "Solo vip");

		Vuelo vuelo2 = new Vuelo(salida, destino, avionTurista, fechaSalida,
				new Float(200), "Solo turista");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "vip", "", 0, 5);

		assertEquals(vuelos.size(), 1);
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());
		assertEquals(true, vuelos.get(0).getNumPlazasLibresVipPas() > 0);
		assertEquals(true, vuelos.get(0).getNumPlazasLibresVipVent() > 0);
	}

	@Test
	public void BuscarVueloClaseVacio() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionTurista1 = new Avion("avioncito", 50, 0);
		avionDao.save(avionTurista1);

		Avion avionTurista2 = new Avion("avioncito2", 50, 0);
		avionDao.save(avionTurista2);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avionTurista1, fechaSalida,
				new Float(200), "");

		Vuelo vuelo2 = new Vuelo(salida, destino, avionTurista2, fechaSalida,
				new Float(200), "");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "vip", "ventana", 0, 5);

		assertEquals(vuelos.size(), 0);
	}

	@Test
	public void BuscarVariosClase() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionTurista1 = new Avion("avioncito", 50, 0);
		avionDao.save(avionTurista1);

		Avion avionTurista2 = new Avion("avioncito2", 50, 0);
		avionDao.save(avionTurista2);

		Avion avionVip = new Avion("avioncito3", 0, 50);
		avionDao.save(avionVip);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avionTurista1, fechaSalida,
				new Float(500), "Comida incluida");

		Vuelo vuelo2 = new Vuelo(salida, destino, avionTurista2, fechaSalida,
				new Float(200), "");

		Vuelo vuelo3 = new Vuelo(salida, destino, avionVip, fechaSalida,
				new Float(200), "");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);
		vueloDao.save(vuelo3);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "turista", "", 0, 5);

		assertEquals(vuelos.size(), 2);
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());

		assertEquals(salida.getNombreCorto(), vuelos.get(1).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(1).getDestino()
				.getNombreCorto());

		assertEquals(vuelo1.getPrecioBase(), vuelos.get(0).getPrecioBase());
		assertEquals(vuelo2.getPrecioBase(), vuelos.get(1).getPrecioBase());

		assertEquals(true, vuelos.get(0).getNumPlazasLibresNormalPas() > 0);
		assertEquals(true, vuelos.get(0).getNumPlazasLibresNormalVent() > 0);
		assertEquals(true, vuelos.get(1).getNumPlazasLibresNormalPas() > 0);
		assertEquals(true, vuelos.get(1).getNumPlazasLibresNormalVent() > 0);

	}

	/********************************* Test*historia*H3 ****************************************************/
	@Test
	public void BuscarVueloClaseYPlazaExistente() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionPasillo = new Avion("avioncito", 0, 50);
		avionDao.save(avionPasillo);
		avionPasillo.setNumPlazasVipPas(0);
		Avion avionVentana = new Avion("avioncito2", 0, 50);
		avionDao.save(avionVentana);
		avionVentana.setNumPlazasVipVent(0);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avionPasillo, fechaSalida,
				new Float(200), "Solo vip");

		Vuelo vuelo2 = new Vuelo(salida, destino, avionVentana, fechaSalida,
				new Float(200), "Solo turista");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "vip", "pasillo", 0, 5);

		assertEquals(vuelos.size(), 1);
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());
		assertEquals(true, vuelos.get(0).getNumPlazasLibresVipPas() > 0);
		assertEquals(true, vuelos.get(0).getNumPlazasLibresNormalPas() == 0);
	}

	@Test
	public void BuscarVueloClaseYPlazaVacio() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionPasillo1 = new Avion("avioncito", 50, 0);
		avionDao.save(avionPasillo1);
		Avion avionPasillo2 = new Avion("avioncito2", 50, 50);
		avionDao.save(avionPasillo2);
		avionPasillo2.setNumPlazasVipVent(0);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avionPasillo1, fechaSalida,
				new Float(200), "");

		Vuelo vuelo2 = new Vuelo(salida, destino, avionPasillo2, fechaSalida,
				new Float(200), "");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "vip", "ventana", 0, 5);

		assertEquals(vuelos.size(), 0);
	}

	@Test
	public void BuscarVariosPlaza() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionPasillo = new Avion("avioncito", 50, 0);
		avionDao.save(avionPasillo);
		avionPasillo.setNumPlazasNormalVent(0);
		Avion avionVentana = new Avion("avioncito2", 0, 50);
		avionDao.save(avionVentana);
		avionVentana.setNumPlazasVipPas(0);
		Avion avionNormal = new Avion("avioncito", 50, 50);
		avionDao.save(avionNormal);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Vuelo vuelo1 = new Vuelo(salida, destino, avionPasillo, fechaSalida,
				new Float(500), "Comida incluida");

		Vuelo vuelo2 = new Vuelo(salida, destino, avionVentana, fechaSalida,
				new Float(200), "");

		Vuelo vuelo3 = new Vuelo(salida, destino, avionNormal, fechaSalida,
				new Float(200), "");

		vueloDao.save(vuelo1);
		vueloDao.save(vuelo2);
		vueloDao.save(vuelo3);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, null, "turista", "pasillo",
				0, 5);

		assertEquals(vuelos.size(), 2);
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());

		assertEquals(salida.getNombreCorto(), vuelos.get(1).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(1).getDestino()
				.getNombreCorto());

		assertEquals(vuelo1.getPrecioBase(), vuelos.get(0).getPrecioBase());
		assertEquals(vuelo2.getPrecioBase(), vuelos.get(1).getPrecioBase());

		assertEquals(true, vuelos.get(0).getNumPlazasLibresNormalPas() > 0);
		assertEquals(true, vuelos.get(1).getNumPlazasLibresNormalPas() > 0);
	}

	/********************************* Test*historia*H4 ****************************************************/
	@Test
	public void BuscarVueloIdaVueltaExistente() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionTurista = new Avion("avioncito", 50, 0);
		avionDao.save(avionTurista);

		Avion avionVip = new Avion("avioncito2", 0, 50);
		avionDao.save(avionVip);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Calendar fechaVuelta = Calendar.getInstance();
		fechaSalida.set(2016, 1, 12);

		Vuelo ida = new Vuelo(salida, destino, avionVip, fechaSalida,
				new Float(200), "Solo vip");

		Vuelo vuelta = new Vuelo(destino, salida, avionTurista, fechaVuelta,
				new Float(200), "Solo turista");

		vueloDao.save(ida);
		vueloDao.save(vuelta);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, fechaVuelta, "", "", 0, 5);

		assertEquals(2, vuelos.size());
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());
		assertEquals(salida.getNombreCorto(), vuelos.get(1).getDestino()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(1).getOrigen()
				.getNombreCorto());
		assertEquals(true, vuelos.get(0).getNumPlazasLibresVipPas() > 0);
		assertEquals(true, vuelos.get(0).getNumPlazasLibresVipVent() > 0);
		assertEquals(true, vuelos.get(1).getNumPlazasLibresNormalPas() > 0);
		assertEquals(true, vuelos.get(1).getNumPlazasLibresNormalVent() > 0);
	}

	@Test
	public void BuscarVueloIdaVueltaVacio() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionTurista = new Avion("avioncito", 50, 0);
		avionDao.save(avionTurista);

		Avion avionVip = new Avion("avioncito2", 0, 50);
		avionDao.save(avionVip);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 11, 12);

		Calendar fechaVuelta = Calendar.getInstance();
		fechaSalida.set(2016, 01, 12);

		Vuelo ida = new Vuelo(salida, destino, avionTurista, fechaSalida,
				new Float(200), "");

		Vuelo vuelta = new Vuelo(destino, salida, avionVip, fechaVuelta,
				new Float(200), "");

		vueloDao.save(ida);
		vueloDao.save(vuelta);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, fechaSalida, "", "", 0, 5);

		assertEquals(vuelos.size(), 0); // Aunque hay un vuelo de ida valido no
										// pasa lo mismo en el
										// caso de la vuelta, por lo que debe de
										// volver que no hay vuelos
	}

	@Test
	public void BuscarVariosIdaVuelta() {
		Aeropuerto salida = new Aeropuerto("Aeropuerto de Madrid",
				"Aeropuerto de Madrid", "España", "Madrid");
		aeropuertoDao.save(salida);
		Aeropuerto destino = new Aeropuerto("Aeropuerto de Barcelona",
				"Aeropuerto de Barcelona", "España", "Barcelona");
		aeropuertoDao.save(destino);

		Avion avionTurista = new Avion("avioncito", 50, 0);
		avionDao.save(avionTurista);

		Avion avionVip = new Avion("avioncito2", 0, 50);
		avionDao.save(avionVip);

		Calendar fechaSalida = Calendar.getInstance();
		fechaSalida.set(2015, 12, 12);

		Calendar fechaVuelta = Calendar.getInstance();
		fechaVuelta.set(2016, 1, 12);

		Calendar fechaOtra = Calendar.getInstance();
		fechaOtra.set(2016, 5, 12);

		Vuelo ida1 = new Vuelo(salida, destino, avionTurista, fechaSalida,
				new Float(500), "Comida incluida");

		Vuelo ida2 = new Vuelo(salida, destino, avionVip, fechaSalida,
				new Float(200), "");

		Vuelo vuelta = new Vuelo(destino, salida, avionVip, fechaVuelta,
				new Float(200), "");

		Vuelo vuelo = new Vuelo(salida, destino, avionTurista, fechaOtra,
				new Float(344), "");

		vueloDao.save(ida1);
		vueloDao.save(ida2);
		vueloDao.save(vuelta);
		vueloDao.save(vuelo);

		List<Vuelo> vuelos = buscador.buscarVuelos(salida.getAeroId(),
				destino.getAeroId(), fechaSalida, fechaVuelta, "", "", 0, 5);

		assertEquals(vuelos.size(), 3);
		assertEquals(salida.getNombreCorto(), vuelos.get(0).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(0).getDestino()
				.getNombreCorto());

		assertEquals(salida.getNombreCorto(), vuelos.get(1).getOrigen()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(1).getDestino()
				.getNombreCorto());

		assertEquals(salida.getNombreCorto(), vuelos.get(2).getDestino()
				.getNombreCorto());
		assertEquals(destino.getNombreCorto(), vuelos.get(2).getOrigen()
				.getNombreCorto());
	}

	/********************************* Test*historia*H5 ****************************************************/
	/********************************* Test*historia*H6 ****************************************************/
}
